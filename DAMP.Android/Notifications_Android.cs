using System.Threading.Tasks;
using DAMP.Portable;
using Xamarin.Forms;
using System;
using System.IO;
using DAMPAndroid;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.Widget;
using DAMPAndroid.Services;

[assembly: Dependency(typeof(Notifications_Android))]

namespace DAMPAndroid
{
    public class Notifications_Android : INotifications
    {
        Notification.Builder builder;
        #region INotifications implementation

        public void StartNotification(string title, string text, int type)
        {
            builder = new Notification.Builder(Android.App.Application.Context);
            builder.SetContentTitle(text)
                .SetLargeIcon(BitmapFactory.DecodeResource(Android.App.Application.Context.Resources, Resource.Drawable.ic_notify));

            

            //type:
            //0 = download
            if (type == 0)
            {
                var intent = new Intent("com.lastersoft.DAMP.TEST");
                intent.PutExtra("someKey", "someValue");

                //var mainActivity = Android.App.Application.Context.PackageManager.GetLaunchIntentForPackage(Android.App.Application.Context.PackageName);
                PendingIntent pendingSwitchIntent = PendingIntent.GetBroadcast(Android.App.Application.Context, 0,
                        intent, 0);

                var intent2 = new Intent(Android.App.Application.Context, typeof(DownloaderService));
                intent2.SetAction("STOP");
                PendingIntent pendingSwitchIntent2 = PendingIntent.GetService(Android.App.Application.Context, 0, intent2, 0);

                builder.SetSmallIcon(Resource.Drawable.ic_notify_dl);
                builder.SetProgress(1000, 0, false);

                var newAction = new Notification.Action.Builder(Resource.Drawable.ic_notify, "Cancel", pendingSwitchIntent2).Build();
                builder.AddAction(newAction);
                builder.SetOngoing(true);
            }

            // Build the notification:
            Notification notification = builder.Build();

            // Get the notification manager:
            NotificationManager notificationManager =
            Android.App.Application.Context.GetSystemService(Context.NotificationService) as NotificationManager;

            // Publish the notification:
            notificationManager.Notify((418+type), notification);
        }

        public void StopNotification(int type)
        {
            NotificationManager notificationManager =
                   Android.App.Application.Context.GetSystemService(Context.NotificationService) as NotificationManager;
            notificationManager.Cancel((418 + type));
        }

        public void UpdateNotification(double progress, int type)
        {
            if (builder != null)
            {
                if (type == 0)
                {
                    builder.SetContentText(progress.ToString() + "%");
                    builder.SetProgress(1000, (int)(progress * 10), false);

                    // Build a notification object with updated content:
                    Notification notification = builder.Build();

                    // Publish the new notification with the existing ID:
                    NotificationManager notificationManager =
                    Android.App.Application.Context.GetSystemService(Context.NotificationService) as NotificationManager;
                    notificationManager.Notify((418 + type), notification);
                }
            }
        }

        #endregion
    }
}