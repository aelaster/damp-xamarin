using System.Threading.Tasks;
using DAMP.Portable;
using Xamarin.Forms;
using System;
using System.IO;
using DAMPAndroid;

[assembly: Dependency(typeof(Files_Android))]

namespace DAMPAndroid
{
    public class Files_Android : IFiles
    {
        #region ISaveAndLoad implementation

        public bool FileExists(string filename)
        {
            return File.Exists(CreatePathToFile(filename));
        }

        public async Task<string> LoadFileAsync(string filename)
        {
            var path = CreatePathToFile(filename);
            using (StreamReader sr = File.OpenText(path))
                return await sr.ReadToEndAsync();
        }

        public async Task SaveFileAsync(string filename, string text)
        {
            var path = CreatePathToFile(filename);
            using (StreamWriter sw = File.CreateText(path))
                await sw.WriteAsync(text);
        }

        public async Task SaveFileAsync(string filename, byte[] data)
        {
            var path = CreatePathToFile(filename);
            await Task.Run(() => File.WriteAllBytes(path, data));
        }

        public string GetDocPath(string filename)
        {
            var docsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            return Path.Combine(docsPath, filename);
        }

        public bool MoveFromTemp(string filename)
        {
            return true;
        }

        #endregion

        string CreatePathToFile(string filename)
        {
            var docsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            return Path.Combine(docsPath, filename);
        }
    }
}