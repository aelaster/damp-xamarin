﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using DAMP.Portable;
using DAMP.Portable.Messages;
using Xamarin.Android.Net;
using Xamarin.Forms;

namespace DAMPAndroid
{
    class FileHelper
    {
        public bool cancelFlag = false;
        string CreatePathToFile(string filename)
        {
            var docsPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            return Path.Combine(docsPath, filename);
        }

        public void CancelMe()
        {
            cancelFlag = true;
        }

        private bool NeedsDownload(string filename)
        {
            return !File.Exists(CreatePathToFile(filename));
        }

        public Task<string> DownloadFileAsync(string url, string filename)
        {
            if (NeedsDownload(filename))
            {
                return Task.Run(() => DownloadFile(url, filename));
            }
            else
            {
                return Task.FromResult(CreatePathToFile(filename));
            }
        }

        private async Task<string> DownloadFile(string url, string filename)
        {
            int receivedBytes = 0;
            long totalBytes = 0;
            HttpClient client = new HttpClient();
            byte[] output = null;
            var fileService = DependencyService.Get<IFiles>();
            var notificationService = DependencyService.Get<INotifications>();
            notificationService.StartNotification("DAMP", filename, 0);

            using (var stream = await client.GetStreamAsync(url))
            {
                byte[] buffer = null;
                totalBytes = stream.Length;
                output = new byte[totalBytes];

                for (; ; )
                {
                    if (cancelFlag)
                    {
                        break;
                    }
                    if (totalBytes > 0 && totalBytes - receivedBytes < 4096)
                    {
                        buffer = new byte[totalBytes - receivedBytes];
                    }
                    else
                    {
                        buffer = new byte[4096];
                    }
                    int bytesRead = await stream.ReadAsync(buffer, 0, buffer.Length);
                    if (bytesRead == 0)
                    {
                        await Task.Yield();
                        break;
                    }

                    buffer.CopyTo(output, receivedBytes);

                    receivedBytes += bytesRead;

                    int received = unchecked((int)receivedBytes);
                    int total = unchecked((int)totalBytes);

                    double percentage = (((float)received) / total);
                    double percentage_out = Math.Round(percentage * 100, 2);

                    notificationService.UpdateNotification(percentage_out, 0);
                    var message = new DownloadProgressMessage()
                    {
                        BytesWritten = bytesRead,
                        TotalBytesExpectedToWrite = totalBytes,
                        TotalBytesWritten = receivedBytes,
                        Percentage = percentage
                    };

                    MessagingCenter.Send(message, "DownloadProgressMessage");
                }
            }

            notificationService.StopNotification(0);

            if (!cancelFlag)
            {
                await fileService.SaveFileAsync(filename, output);
                return CreatePathToFile(filename);
            }
            else
            {
                return "Cancelled";
            }
        }
    }
}