﻿using Android.App;
using Android.Views;
using Android.OS;
using Xamarin.Forms.Platform.Android;
using Xamarin.Forms;
using Android.Content.PM;
using DAMP.Portable;
using ImageCircle.Forms.Plugin.Droid;
using Microsoft.AppCenter.Push;
using System.Threading.Tasks;
using Android.Content;
using Android.Support.V7.App;

namespace DAMPAndroid
{
    [Activity(Theme = "@style/MyTheme.Splash", MainLauncher = true, NoHistory = true)]
    public class SplashActivity : AppCompatActivity
    {
        public override void OnCreate(Bundle savedInstanceState, PersistableBundle persistentState)
        {
            base.OnCreate(savedInstanceState, persistentState);
        }

        // Launches the startup task
        protected override void OnResume()
        {
            base.OnResume();
            Task startupWork = new Task(() => { SimulateStartup(); });
            startupWork.Start();
        }

        // Simulates background work that happens behind the splash screen
        async void SimulateStartup()
        {
            StartActivity(new Intent(this, typeof(MainActivity)));
        }

        public override void OnBackPressed() { }
    }
}
