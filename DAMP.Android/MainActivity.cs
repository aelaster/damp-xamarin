﻿using Android.App;
using Android.Views;
using Android.OS;
using Xamarin.Forms.Platform.Android;
using Xamarin.Forms;
using Android.Content.PM;
using DAMP.Portable;
using ImageCircle.Forms.Plugin.Droid;
using Microsoft.AppCenter.Push;
using DAMP.Portable.Messages;
using Android.Content;
using DAMPAndroid.Services;
using CarouselView.FormsPlugin.Android;

namespace DAMPAndroid
{
    [Activity(Label = "Drew and Mike Podcast",
        ScreenOrientation = ScreenOrientation.Portrait,
        ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : FormsAppCompatActivity
    {
        public Binder myBinder;
        public bool isBound = false;
        DownloaderServiceConnection dlServiceConnection;

        protected override void OnCreate(Bundle bundle)
        {
            FormsAppCompatActivity.ToolbarResource = Resource.Layout.Toolbar;
            FormsAppCompatActivity.TabLayoutResource = Resource.Layout.Tabbar;
            base.OnCreate(bundle);
            Forms.Init(this, bundle);
            CarouselViewRenderer.Init();
            //Plugin.MediaManager.Forms.Android.VideoViewRenderer.Init();
            ImageCircleRenderer.Init();
            Push.SetSenderId("3721589968");
            App.CreateAudioPlayer = () => new SimpleAudioPlayer();
            LoadApplication(new App());
            Window.SetStatusBarColor(Android.Graphics.Color.Argb(255, 0, 0, 0));

            MessagingCenter.Subscribe<DownloadMessage>(this, "Download", message => {
                var intent = new Intent(this, typeof(DownloaderService));
                intent.PutExtra("url", message.Url);
                intent.PutExtra("filename", message.Filename);
                StartService(intent);
            });
        }
    }
}
