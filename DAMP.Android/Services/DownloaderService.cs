﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using DAMP.Portable.Messages;
using Xamarin.Forms;

namespace DAMPAndroid.Services
{
    [Service]
    public class DownloaderService : Service
    {
        private CancellationTokenSource _cts;
        private FileHelper fileHelper;
        public override IBinder OnBind(Intent intent)
        {
            //return new DownloaderBinder(this);
            return null;
        }

        public override StartCommandResult OnStartCommand(Intent intent, StartCommandFlags flags, int startId)
        {
            if(intent.Action == "STOP")
            {
                try
                {
                    fileHelper.CancelMe();
                    _cts.Cancel();     // cancel previous search
                    StopSelf();
                }
                catch (ObjectDisposedException)     // in case previous search completed
                {

                }
            }
            else
            {
                var url = intent.GetStringExtra("url");
                var filename = intent.GetStringExtra("filename");
                try
                {
                    using (_cts = new CancellationTokenSource())
                    {
                        Task.Run(() =>
                        {
                            fileHelper = new FileHelper();
                            fileHelper.DownloadFileAsync(url, filename)
                                .ContinueWith(filePath =>
                                {
                                    var message = new DownloadFinishedMessage
                                    {
                                        FilePath = filePath.Result
                                    };
                                    MessagingCenter.Send(message, "DownloadFinishedMessage");
                                });
                        }, _cts.Token);
                    }
                }
                catch (TaskCanceledException)       // if the operation is cancelled, do nothing
                {
                }
            }
            return StartCommandResult.Sticky;
        }
    }
}