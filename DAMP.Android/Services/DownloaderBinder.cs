﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace DAMPAndroid.Services
{
    public class DownloaderBinder : Binder
    {
        DownloaderService service;

        public DownloaderBinder(DownloaderService service)
        {
            this.service = service;
        }

        public DownloaderService GetDownloaderService()
        {
            return service;
        }
    }
}