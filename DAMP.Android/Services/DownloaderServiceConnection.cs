﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace DAMPAndroid.Services
{
    class DownloaderServiceConnection : Java.Lang.Object, IServiceConnection
    {
        MainActivity activity;

        public DownloaderServiceConnection(MainActivity activity)
        {
            this.activity = activity;
        }

        public void OnServiceConnected(ComponentName name, IBinder service)
        {
            var dlServiceBinder = service as DownloaderBinder;
            if (dlServiceBinder != null)
            {
                activity.myBinder = dlServiceBinder;
                activity.isBound = true;
            }
        }

        public void OnServiceDisconnected(ComponentName name)
        {
            activity.isBound = false;
        }
    }
}