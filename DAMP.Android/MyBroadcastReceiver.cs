﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using DAMPAndroid.Services;

namespace DAMPAndroid
{
    [BroadcastReceiver(Enabled = true)]
    [IntentFilter(new[] { "com.lastersoft.DAMP.TEST" })]
    public class MyBroadcastReceiver : BroadcastReceiver
    {
        public override void OnReceive(Context context, Intent intent)
        {
            Toast.MakeText(context, "Received intent!", ToastLength.Short).Show();
            var intenty = new Intent(context, typeof(DownloaderService));
            context.StopService(intenty);
        }
    }
}