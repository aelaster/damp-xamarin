﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Collections.ObjectModel;
using System.Net.Http;
using System.Xml.Linq;

namespace DAMP.Portable.ViewModels
{
    public class PodcastPlaybackViewModel : BaseViewModel
    {
        private string content = string.Empty;
        public string Content
        {
            get { return content; }
            set { SetProperty(ref content, value); }
        }

        private string image = string.Empty;
        public string Image
        {
            get { return image; }
            set { SetProperty(ref image, value); }
        }

        private string publish_date = string.Empty;
        public string PublishDate
        {
            get { return publish_date; }
            set { SetProperty(ref publish_date, value); }
        }

        private FeedItem feed_item = null;
        public FeedItem Item
        {
            get { return feed_item; }
            set { SetProperty(ref feed_item, value); }
        }

        public class CurrentPodcast
        {
            public FeedItem Item { get; set; }
            public string Image { get; set; }
            public string Content { get; set; }
            public string Title { get; set; }
            public string PublishDate { get; set; }
        }

        ObservableCollection<CurrentPodcast> _myItemsSource { get; set; }
        public ObservableCollection<CurrentPodcast> MyItemsSource
        {
            set
            {
                _myItemsSource = value;
                OnPropertyChanged("MyItemsSource");
            }
            get
            {
                return _myItemsSource;
            }
        }

        public PodcastPlaybackViewModel(FeedItem item)
        {
            Item = item;
            Image = item.Image;
            Content = item.Content;
            Title = item.Title;
            PublishDate = item.PublishDate;
            MyItemsSource = new ObservableCollection<CurrentPodcast>()
            {
                new CurrentPodcast
                {
                    Item = item,
                    Image = item.Image,
                    Content = item.Description,
                    Title = item.Title,
                    PublishDate = item.PublishDate
                }
            };
        }
    }
}
