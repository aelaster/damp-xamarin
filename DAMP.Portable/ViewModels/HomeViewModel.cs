﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Input;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace DAMP.Portable
{
    public class HomeViewModel : BaseViewModel
    {
        public ObservableCollection<HomeMenuItem> MenuItems { get; set; }
        public HomeViewModel()
        {
            CanLoadMore = true;
            Title = "Drew & Mike";
            MenuItems = new ObservableCollection<HomeMenuItem>();
            MenuItems.Add(new HomeMenuItem
            {
                Id = 0,
                Title = "Podcast",
                MenuType = MenuType.DrewMikePodcast,
                Icon = "newshowlogo.jpg"
            });
            MenuItems.Add(new HomeMenuItem
            {
                Id = 1,
                Title = "Twitter",
                MenuType = MenuType.Twitter,
                Icon = "twitternav.png"
            });
            MenuItems.Add(new HomeMenuItem
            {
                Id = 2,
                Title = "Drops",
                MenuType = MenuType.Drops,
                Icon = "newshowlogo.jpg"
            });
            MenuItems.Add(new HomeMenuItem
            {
                Id = 3,
                Title = "Sponsors",
                MenuType = MenuType.Sponsors,
                Icon = "newshowlogo.jpg"
            });
        }
    }
}

