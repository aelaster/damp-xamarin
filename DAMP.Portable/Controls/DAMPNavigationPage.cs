﻿using System;
using Xamarin.Forms;

namespace DAMP.Portable.Controls
{
    public class DAMPNavigationPage :NavigationPage
    {
        public DAMPNavigationPage(Page root) : base(root)
        {
            Init();
            
        }

        public DAMPNavigationPage()
        {
            Init();
        }

        void Init()
        {
            BackgroundColor = Color.Transparent;
            BarBackgroundColor = Color.FromHex("#FF5C02");
            BarTextColor = Color.White;
        }
    }
}

