﻿using System.Threading.Tasks;

namespace DAMP.Portable
{
    public interface INotifications
    {
        void StartNotification(string title, string text, int type);
        void StopNotification(int type);
        void UpdateNotification(double progress, int type);
    }
}