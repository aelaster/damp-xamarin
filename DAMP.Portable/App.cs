﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using DAMP.Portable.Views;
using Microsoft.AppCenter;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;
using Microsoft.AppCenter.Push;
using SimpleAudio;
using Xamarin.Forms.Xaml;

namespace DAMP.Portable
{
    public class App : Application
    {
        public static Func<ISimpleAudioPlayer> CreateAudioPlayer { get; set; }
        public static bool IsWindows10 {get;set;}
        public App()
        {
            // The root page of your application
            MainPage = new RootPage();
            MainPage.SetValue(NavigationPage.BarBackgroundColorProperty, Color.Black);
        }

        protected override void OnStart()
        {
            // Handle when your app starts
            AppCenter.Start("android=32076337-3904-4fc9-9150-ad54d28bd6ec;" +
                  "ios=b6f35c14-a473-43f4-9aa6-ce4150e66c7d;",
                  typeof(Analytics), typeof(Crashes), typeof(Push));
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
