﻿using System;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;

namespace DAMP.Portable.Views
{
    public partial class MenuPage : ContentPage
    {
        public MenuPage()
        {
            InitializeComponent();
        }

        RootPage root;
        List<HomeMenuItem> menuItems;
        public MenuPage(RootPage root)
        {
            
            this.root = root;
            InitializeComponent();
            if (!App.IsWindows10)
            {
                BackgroundColor = Color.FromHex("#FF5C02");
                ListViewMenu.BackgroundColor = Color.FromHex("#F5F5F5");
            }

            BindingContext = new BaseViewModel
            {
                Title = "Drew & Mike Podcast",
                Subtitle= "Drew & Mike Podcast",
                Icon = "slideout.png"
            };

            ListViewMenu.ItemsSource = menuItems = new List<HomeMenuItem>
            {
                new HomeMenuItem { Title = "Podcast", MenuType = MenuType.DrewMikePodcast, Icon = "newshowlogo.jpg"},
                new HomeMenuItem { Title = "Twitter", MenuType = MenuType.Twitter, Icon = "twitternav.png" },
                new HomeMenuItem { Title = "Drops", MenuType = MenuType.Drops, Icon = "newshowlogo.jpg"},
                new HomeMenuItem { Title = "Sponsors", MenuType = MenuType.Sponsors, Icon = "newshowlogo.jpg"},
            };

            ListViewMenu.SelectedItem = menuItems?.FirstOrDefault();

            ListViewMenu.ItemSelected += async (sender, e) => 
            {
                if(ListViewMenu.SelectedItem == null)
                    return;
                ListViewMenu.SelectedItem = null;
                await this.root.NavigateAsync((int)((HomeMenuItem)e.SelectedItem).MenuType);
            };
        }
    }
}

