﻿using System;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Collections.Generic;
using DAMP.Portable.Controls;

namespace DAMP.Portable.Views
{
    public class RootPage : MasterDetailPage
    {
        public static bool IsUWPDesktop { get; set; }
        Dictionary<int, NavigationPage> Pages { get; set;} 
        public RootPage()
        {
            if(IsUWPDesktop)
                this.MasterBehavior = MasterBehavior.Popover;

            Pages = new Dictionary<int, NavigationPage>();
            Master = new MenuPage(this);
            BindingContext = new BaseViewModel
                {
                    Title = "Drew & Mike",
                    Icon = "slideout.png"
                };
            //setup home page
    
            Pages.Add((int)MenuType.DrewMikePodcast, new DAMPNavigationPage(new PodcastPage((MenuType)0, this)));
            Detail = Pages[(int)MenuType.DrewMikePodcast];

            InvalidateMeasure();
        }

        public void SetGesture(bool flag)
        {
            IsGestureEnabled = flag;
        }

        public async Task NavigateAsync(int id)
        {
            if (Detail != null)
            {
                if (IsUWPDesktop || Device.Idiom != TargetIdiom.Tablet)
                    IsPresented = false;

                if (Device.RuntimePlatform == Device.Android)
                    await Task.Delay(300);
            }

            Page newPage;
            if (!Pages.ContainsKey(id))
            {
                switch (id)
                {
                    case (int)MenuType.DrewMikePodcast:
                        Pages.Add(id, new DAMPNavigationPage(new PodcastPage((MenuType)id, this)));
                        break;
                    case (int)MenuType.Twitter:
                        Pages.Add(id, new DAMPNavigationPage(new TwitterPage()));
                        break;
                    case (int)MenuType.Drops:
                        Pages.Add(id, new DAMPNavigationPage(new DropsPage()));
                        break;
                    case (int)MenuType.Sponsors:
                        Pages.Add(id, new DAMPNavigationPage(new SponsorsPage()));
                        break;
                }
            }

            newPage = Pages[id];
            if(newPage == null)
                return;

            //pop to root for Windows Phone
            if (Detail != null && Device.RuntimePlatform == Device.WinPhone)
            {
                await Detail.Navigation.PopToRootAsync();
            }

            Detail = newPage;
        }
    }
}

