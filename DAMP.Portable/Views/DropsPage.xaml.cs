﻿using SimpleAudio;
using System;
using System.IO;
using System.Reflection;
using Xamarin.Forms;

namespace DAMP.Portable.Views
{
    public partial class DropsPage : ContentPage
    {
        enum DrumType
        {
            TomTom,
            Snare,
            Bass,
            HiHat,
            count
        }

        ISimpleAudioPlayer[] players = new ISimpleAudioPlayer[(int)DrumType.count];
        Animation[] animations = new Animation[(int)DrumType.count + 1];

        public DropsPage()
        {
            for (int i = 0; i < (int)DrumType.count; i++)
            {
                players[i] = App.CreateAudioPlayer();
            }

            InitializeComponent();

            Color colorButton = btnPlayBass.BackgroundColor;
            Color colorHighlight = Color.FromHex("#000000");

            animations[(int)DrumType.Bass] = new Animation(v => btnPlayBass.BackgroundColor = GetBlendedColor(colorButton, colorHighlight, v), 0, 1);
            animations[(int)DrumType.HiHat] = new Animation(v => btnPlayHiHat.BackgroundColor = GetBlendedColor(colorButton, colorHighlight, v), 0, 1);
            animations[(int)DrumType.Snare] = new Animation(v => btnPlaySnare.BackgroundColor = GetBlendedColor(colorButton, colorHighlight, v), 0, 1);
            animations[(int)DrumType.TomTom] = new Animation(v => btnPlayTomTom.BackgroundColor = GetBlendedColor(colorButton, colorHighlight, v), 0, 1);
            animations[(int)DrumType.count] = new Animation(v => btnSTFU.BackgroundColor = GetBlendedColor(colorButton, colorHighlight, v), 0, 1);

            btnPlayBass.Clicked += (s, e) => OnDrumButton(DrumType.Bass);
            btnPlaySnare.Clicked += (s, e) => OnDrumButton(DrumType.Snare);
            btnPlayTomTom.Clicked += (s, e) => OnDrumButton(DrumType.TomTom);
            btnPlayHiHat.Clicked += (s, e) => OnDrumButton(DrumType.HiHat);

            btnSTFU.Clicked += (s, e) =>
            {
                OnCancelButton(DrumType.HiHat);
                OnCancelButton(DrumType.TomTom);
                OnCancelButton(DrumType.Snare);
                OnCancelButton(DrumType.Bass);
            };
        }

        void PickerKitsSelectedIndexChanged(object sender, EventArgs e)
        {
            var picker = sender as Picker;

            LoadSamples(picker.SelectedIndex + 1);
        }

        void OnCancelButton(DrumType drumType)
        {
            Color colorButton = btnPlayBass.BackgroundColor;
            if (players[(int)drumType].IsPlaying()) { 
                players[(int)drumType]?.Cancel();
                switch (drumType)
                {
                    case DrumType.TomTom:
                        btnPlayTomTom.BackgroundColor = colorButton;
                        break;
                    case DrumType.Snare:
                        btnPlaySnare.BackgroundColor = colorButton;
                        break;
                    case DrumType.Bass:
                        btnPlayBass.BackgroundColor = colorButton;
                        break;
                    case DrumType.HiHat:
                        btnPlayHiHat.BackgroundColor = colorButton;
                        break;
                }
            }
            animations[(int)DrumType.count].Commit(this, drumType.ToString());
        }

        void OnDrumButton(DrumType drumType)
        {
            players[(int)drumType]?.Play();
            animations[(int)drumType]?.Commit(this, drumType.ToString());
        }

        void LoadSamples(int index)
        {
            if (index < 1 || index > 6)
                return;

            index = index + 2;

            players[(int)DrumType.Bass].Load(GetStreamFromFile($"Audio.bd{index}.wav"));
            players[(int)DrumType.Snare].Load(GetStreamFromFile($"Audio.sd{index}.wav"));
            players[(int)DrumType.TomTom].Load(GetStreamFromFile($"Audio.tt{index}.wav"));
            players[(int)DrumType.HiHat].Load(GetStreamFromFile($"Audio.hh{index}.wav"));

            switch (index)
            {
                case 3:
                    //trump shit
                    btnPlayTomTom.Text = "Fake News";
                    btnPlaySnare.Text = "Outta Here";
                    btnPlayBass.Text = "Stop It";
                    btnPlayHiHat.Text = "Failure";
                    break;
                case 4:
                    //trump shit 2
                    btnPlayTomTom.Text = "Boring";
                    btnPlaySnare.Text = "Eh Boy";
                    btnPlayBass.Text = "All a Lie";
                    btnPlayHiHat.Text = "Out!";
                    break;
                case 5:
                    //drew and mike shit
                    btnPlayTomTom.Text = "OOOOOOOOO!";
                    btnPlaySnare.Text = "Garbage";
                    btnPlayBass.Text = "Adobe Sofuhlare";
                    btnPlayHiHat.Text = "2nd and 9";
                    break;
                case 6:
                    //drew and mike shit 2
                    btnPlayTomTom.Text = "I Agree";
                    btnPlaySnare.Text = "Sayonara";
                    btnPlayBass.Text = "Stop Whining";
                    btnPlayHiHat.Text = "Stop It";
                    break;
                default:
                    btnPlayTomTom.Text = "TomTom";
                    btnPlaySnare.Text = "Snare";
                    btnPlayBass.Text = "Bass";
                    btnPlayHiHat.Text = "HiHat";
                    break;
            }
        }

        Stream GetStreamFromFile(string filename)
        {
            var assembly = typeof(App).GetTypeInfo().Assembly;
            var stream = assembly.GetManifestResourceStream("DAMP.Portable." + filename);

            return stream;
        }

        Color GetBlendedColor(Color color1, Color color2, double percentage)
        {
            return new Color(percentage * color1.R + (1 - percentage) * color2.R,
                             percentage * color1.G + (1 - percentage) * color2.G,
                             percentage * color1.B + (1 - percentage) * color2.B);
        }
    }
}

