﻿using DAMP.Portable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Plugin.Share;
using DAMP.Portable.Helpers;

namespace DAMP.Portable.Views
{
    public partial class SponsorsPage : ContentPage
    {

        /*void OpenBrowser(string url)
        {
            CrossShare.Current.OpenBrowser(url, new Plugin.Share.Abstractions.BrowserOptions
            {
                ChromeShowTitle = true,
                ChromeToolbarColor = new Plugin.Share.Abstractions.ShareColor { R = 3, G = 169, B = 244, A = 255 },
                UseSafariReaderMode = true,
                UseSafariWebViewController = true
            });
        }*/
        public SponsorsPage()
        {
            {
                var browser = new WebView();
                browser.Navigating += (s, e) =>
                {
                    if (e.Url.StartsWith("http"))
                    {
                        try
                        {
                            var uri = new Uri(e.Url);
                            Device.OpenUri(uri);
                        }
                        catch (Exception)
                        {
                        }

                        e.Cancel = true;
                    }
                };

                var htmlSource = new HtmlWebViewSource();

                htmlSource.Html = @"<html><body width=""100%""><table width =""100%""><tr><td width=""100%""><p align=""center""><a href=""http://DealsInTheD.Com"" target=""_blank""><img width=""400"" height=""400"" src=""http://drewandmikepodcast.com/wp-content/uploads/2018/03/ef6871e8-deals-in-the-d-website-banner-feb-2017.png.pagespeed.ce.MXS6Ch4v6T.png""/></a>
                                    <br /><br />
                                    <a href=""http://www.DrRoche.Com"" target=""_blank""><img width=""400"" height=""400"" src=""http://drewandmikepodcast.com/wp-content/uploads/2018/03/x025670d3-dr-roche-feb-2017-05.jpg.pagespeed.ic.r4z62VFrrw.jpg"" /></a>
                                    <br /><br />
                                    <a href=""http://drewandmike.hallfg.com"" target=""_blank""><img width=""400"" height=""203"" src=""http://drewandmikepodcast.com/wp-content/uploads/2018/03/xb3d4af85-admin-ajax.php_.jpeg.pagespeed.ic.QogMFoYimU.jpg""/></a>
                                    <br /><br />
                                     
                                     Be done with glasses and contacts and also have the best vision of your life! Call 1-800-398-EYES<br>
                                    <a href=""http://yaldoeyecenter.com/"" target=""_blank""><img width=""400"" src=""http://drewandmikepodcast.com/wp-content/uploads/2018/03/yaldo_logo-white.png.pagespeed.ce.tFzgUx74V4.png""/></a>
                                     
                                    <br /><br />
                                    <a href=""http://www.pinnaclewealthstrategies.com/"" target=""_blank""><img width=""400"" height=""142"" src=""http://drewandmikepodcast.com/wp-content/uploads/2018/03/x5e37dc3a-pws-e1520041851697.jpg.pagespeed.ic.7ec_eBkInD.jpg""/></a>
                                    <br /><br />
                                    <a href=""http://www.goldstarlaw.com/"" target=""_blank""><img width=""400"" height=""190"" src=""http://drewandmikepodcast.com/wp-content/uploads/2018/03/xebba300e-gold-star-law-logo-e1520042874590.png.pagespeed.ic.eeQR2sSAUS.png""/></a>
                                    <br /><br />
                                    <a href=""https://www.prudentialalarms.com/"" target=""_blank""><img width=""281"" height=""235"" src=""http://drewandmikepodcast.com/wp-content/uploads/2018/03/x4bdfb929-prudential-alarms-logo.png.pagespeed.ic.rq8_FB0cbG.png""/></a>
                                    <br /><br />
                                    <a href=""https://www.amazon.com/?tag=drewmi-20&linkCode=ur1"" target=""_blank"">Drew &#038; Mike Amazon Store &#8211; USA &#8211;<br>We are a participant in the Amazon Services LLC Associates Program, an affiliate advertising program designed to provide a means for us to earn fees by linking to Amazon.com and affiliated sites.<br>Click HERE to purchase.</a>​
                                    </td></tr></table></body></html>";
                browser.Source = htmlSource;
                Content = browser;
                Padding = 0;
                Title = "Sponsors";
            }
            //InitializeComponent();

            /*
            twitter.GestureRecognizers.Add(new TapGestureRecognizer()
            {
                Command = new Command(() =>
                {
                    //try to launch twitter or tweetbot app, else launch browser
                    var launch = DependencyService.Get<ILaunchTwitter>();
                    if(launch == null || !launch.OpenUserName("sDAMP"))
                        OpenBrowser("http://m.twitter.com/DrewMikePodcast");
                })
            });

            facebook.GestureRecognizers.Add(new TapGestureRecognizer()
            {
                Command = new Command(() => OpenBrowser("https://m.facebook.com/DrewMikePodcast"))
            });


            instagram.GestureRecognizers.Add(new TapGestureRecognizer()
            {
                Command = new Command(() => OpenBrowser("https://www.instagram.com/DrewMikePodcast"))
            });
            */



        }
    }
}
