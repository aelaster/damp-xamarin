﻿using Plugin.Share;
using Plugin.MediaManager;
using System.Collections.Generic;
using Plugin.MediaManager.Abstractions;
using Plugin.MediaManager.Abstractions.Enums;
using Plugin.MediaManager.Abstractions.Implementations;
using Xamarin.Forms;
using Plugin.HttpTransferTasks;

using DAMP.Portable.Messages;
using DAMP.Portable.ViewModels;
using System;

namespace DAMP.Portable.Views
{
  public partial class PodcastPlaybackPage : ContentPage
  {
    private IPlaybackController PlaybackController => CrossMediaManager.Current.PlaybackController;

    private void SubscribeToMessages()
    {
        MessagingCenter.Subscribe<DownloadProgressMessage>(this, "DownloadProgressMessage", message => {
            Device.BeginInvokeOnMainThread(() => {
                PlaybackStatus.Text = message.Percentage.ToString("P2");
                ProgressBar.Progress = message.Percentage;
            });
        });

        MessagingCenter.Subscribe<DownloadFinishedMessage>(this, "DownloadFinishedMessage", message => {
            Device.BeginInvokeOnMainThread(() =>
            {
                ProgressBar.Progress = 0;
                Slider.Value = 0;
                if (message.FilePath == "Cancelled")
                {
                        PlaybackStatus.Text = "Cancelled";
                }
                download.IsVisible = false;
                Controls.IsVisible = true;
                Controls2.IsVisible = true;
                ProgressBar.IsVisible = false;
                Slider.IsVisible = true;
                var fileService = DependencyService.Get<IFiles>();
                if (fileService.FileExists(fileName))
                {
                    currentItem.IsDownloaded = true;
                }
            });
            MessagingCenter.Unsubscribe<DownloadFinishedMessage>(this, "DownloadFinishedMessage");
            MessagingCenter.Unsubscribe<DownloadProgressMessage>(this, "DownloadProgressMessage");
            if(message.FilePath != "Cancelled"){
                CrossMediaManager.Current.Play("file://" + message.FilePath);
            }  
        });
    }

    public PodcastPlaybackPage()
    {
        InitializeComponent();
        SubscribeToMessages();
    }

    protected override void OnDisappearing()
    {
        base.OnDisappearing();
        //root.SetGesture(true);
        //PlaybackController.Stop();
        CrossHttpTransfers.Current.CancelAll();
    }


    private double CurrentStreamingPosition = 0;
    private string audioPath = "";
    private string fileName = "";
    private FeedItem currentItem = null;
    private Boolean beingScrubbed = false;
    //private RootPage root = null;
    private float playbackSpeed = 1;
    private Boolean loadingPlayback = false;

    public PodcastPlaybackPage(FeedItem item, RootPage root)
    {
        InitializeComponent();
        BindingContext = new PodcastPlaybackViewModel(item);
        currentItem = item;
        audioPath = item.Mp3Url;
        string[] words = audioPath.Split('/');
        fileName = words[words.Length - 1];
                
        //hide this progress bar for now
        ProgressBar.IsVisible = false;
        Slider.IsEnabled = false;

        //this.root = root;
        //root.SetGesture(false);
        var desc = item.Description;

        var share = new ToolbarItem
        {
            Icon = "ic_share.png",
            Text = "Share",
            Command = new Command(() =>
            {
                CrossShare.Current.Share(new Plugin.Share.Abstractions.ShareMessage
                {
                    Text = "Listening to @DrewMikePodcast's " + item.Title,
                    Title = "Share",
                    Url = item.Link
                });
            })
        };

        ToolbarItems.Add(share);

        myCarousel.ItemsSource = new List<DataTemplate>()
        {
            new DataTemplate(() => { 
                return new Image { Source = item.Image };     
            }),
            new DataTemplate(() => { 
                var myWebView = new WebView
                {
                    Source = new HtmlWebViewSource
                    {
                        Html = item.Content
                    }
                };
                myWebView.Navigating += (s, e) =>
                {
                    if (e.Url.StartsWith("http", StringComparison.CurrentCulture))
                    {
                        try
                        {
                            var uri = new Uri(e.Url);
                            Device.OpenUri(uri);
                        }
                        catch (Exception)
                        {
                        }

                        e.Cancel = true;
                    }
                };
                return myWebView;
            })
        };

        if (Application.Current.Properties.ContainsKey("nowPlaying"))
        {
            System.Diagnostics.Debug.WriteLine("nowPlaying = " + Application.Current.Properties["nowPlaying"] as string);
            System.Diagnostics.Debug.WriteLine("fileName = " + fileName);
            if (Application.Current.Properties["nowPlaying"] as string != fileName)
            {
                //we're playing a different file
                play.Clicked += (sender, args) =>
                {
                    CrossMediaManager.Current.Stop();
                    var fileService = DependencyService.Get<IFiles>();
                    PlaybackStatus.Text = "Loading...";
                    loadingPlayback = true;
                    if (fileService.FileExists(fileName))
                    {
                        CrossMediaManager.Current.Play("file://" + fileService.GetDocPath(fileName));
                    }
                    else
                    {
                        CrossMediaManager.Current.Play(new MediaFile(audioPath, MediaFileType.Audio));
                    }
                    PlaybackController.SetSpeed(playbackSpeed);
                    Application.Current.Properties["nowPlaying"] = fileName;
                    Slider.IsEnabled = true;
                    SetUpControls();
                };
            }
            else
            {
                //we're playing this file
                SetUpControls();
            }
        }
        else
        {
            SetUpControls();
        }

        if (Device.RuntimePlatform == Device.Android || Device.RuntimePlatform == Device.WinPhone || App.IsWindows10)
        {
            play.Text = "Play";
            pause.Text = "Pause";
            stop.Text = "Stop";
            download.Text = "D/L";
            replay.Text = "Back 30s";
            skip.Text = "Skip 30s";
        }

        if((Device.RuntimePlatform == Device.WinPhone || Device.RuntimePlatform == Device.UWP) && !App.IsWindows10)
        {
            BackgroundColor = Color.White;
            title.TextColor = Color.Black;
            date.TextColor = Color.Black;
            play.TextColor = Color.Black;
            pause.TextColor = Color.Black;
            stop.TextColor = Color.Black;
        }
    }

    private void SetUpControls()
    {
        var fileService = DependencyService.Get<IFiles>();

        if (CrossMediaManager.Current.Status == MediaPlayerStatus.Playing)
        {
            PlaybackStatus.Text = "Playing";
        }

        CrossMediaManager.Current.PlayingChanged += (sender, args) =>
        {
            if (!beingScrubbed)
            {
                if (Device.RuntimePlatform == Device.Android)
                {
                    ProgressBar.Progress = args.Progress / 100;
                }
                else
                {
                    ProgressBar.Progress = args.Progress;
                }
            }

            CurrentStreamingPosition = args.Position.TotalSeconds;
            TimeSpan time = TimeSpan.FromSeconds(CurrentStreamingPosition);
            PlayTime.Text = time.ToString(@"hh\:mm\:ss");
            if (loadingPlayback)
            {
                PlaybackStatus.Text = "Playing";
                loadingPlayback = false;
            }
        };

        Slider.ValueChanged += (sender, args) =>
        {
            beingScrubbed = true;
            if (args.NewValue - args.OldValue < 0 || args.NewValue - args.OldValue > .001)
            {
                var totalSeconds = CrossMediaManager.Current.Duration.TotalSeconds;
                PlaybackController.SeekTo(args.NewValue * totalSeconds);
            }
            beingScrubbed = false;
        };

        play.Clicked += (sender, args) =>
        {
            if (CrossMediaManager.Current.Status == MediaPlayerStatus.Paused)
            {
                PlaybackController.Play();
                PlaybackController.SetSpeed(playbackSpeed);
                PlaybackStatus.Text = "Playing";
            }
            else if (CrossMediaManager.Current.Status == MediaPlayerStatus.Stopped)
            {
                PlaybackStatus.Text = "Loading...";
                loadingPlayback = true;
                if (fileService.FileExists(fileName))
                {
                    CrossMediaManager.Current.Play("file://" + fileService.GetDocPath(fileName));
                }
                else
                {
                    CrossMediaManager.Current.Play(new MediaFile(audioPath, MediaFileType.Audio));
                }
                PlaybackController.SetSpeed(playbackSpeed);
                Application.Current.Properties["nowPlaying"] = fileName;
                Slider.IsEnabled = true;
            }
            else
            {
                switch (PlaySpeed.Text)
                {
                    case "1x":
                        playbackSpeed = (float)1.5;
                        PlaybackController.SetSpeed(playbackSpeed);
                        PlaySpeed.Text = "1.5x";
                        break;
                    case "1.5x":
                        playbackSpeed = 2;
                        PlaybackController.SetSpeed(playbackSpeed);
                        PlaySpeed.Text = "2x";
                        break;
                    case "2x":
                        playbackSpeed = (float)0.5;
                        PlaybackController.SetSpeed(playbackSpeed);
                        PlaySpeed.Text = "0.5x";
                        break;
                    default:
                        playbackSpeed = 1;
                        PlaybackController.SetSpeed(playbackSpeed);
                        PlaySpeed.Text = "1x";
                        break;
                }
            }
        };

        pause.Clicked += (sender, args) =>
        {
            if (CrossMediaManager.Current.Status == MediaPlayerStatus.Paused)
            {
                PlaybackController.Play();
                PlaybackController.SetSpeed(playbackSpeed);
                PlaybackStatus.Text = "Playing";
            }
            else
            {
                PlaybackController.Pause();
                PlaybackStatus.Text = "Paused";
            }
        };

        stop.Clicked += (sender, args) =>
        {
            if (CrossMediaManager.Current.Status == MediaPlayerStatus.Playing || CrossMediaManager.Current.Status == MediaPlayerStatus.Paused)
            {
                PlaybackController.Pause();
                PlaybackController.SeekToStart();
                ProgressBar.Progress = 0;
                Slider.Value = 0;
                PlaybackStatus.Text = "Stopped";
                PlayTime.Text = "00:00:00";
                Application.Current.Properties["nowPlaying"] = null;
                Slider.IsEnabled = false;
            }
        };

        skip.Clicked += (sender, args) =>
        {
            if (CrossMediaManager.Current.Status == MediaPlayerStatus.Playing)
            {
                PlaybackController.StepForward();
            }
        };

        replay.Clicked += (sender, args) =>
        {
            if (CrossMediaManager.Current.Status == MediaPlayerStatus.Playing)
            {
                PlaybackController.StepBackward();
            }
        };

        if (fileService.FileExists(fileName))
        {
            download.IsVisible = false;
        }

        download.Clicked += async (sender, args) =>
        {
            SubscribeToMessages();

            if (CrossMediaManager.Current.Status == MediaPlayerStatus.Playing)
            {
                await PlaybackController.Stop();
            }

            if (fileService.FileExists(fileName))
            {
                await CrossMediaManager.Current.Play("file://" + fileService.GetDocPath(fileName));
                await PlaybackController.SetSpeed(playbackSpeed);
                PlayTime.Text = "00:00:00";
            }
            else
            {
                Slider.IsVisible = false;
                ProgressBar.IsVisible = true;
                Controls.IsVisible = false;
                Controls2.IsVisible = false;

                var message = new DownloadMessage
                {
                    Url = audioPath,
                    Filename = fileName
                };
                MessagingCenter.Send(message, "Download");
            }
        };
    }
  }
}
