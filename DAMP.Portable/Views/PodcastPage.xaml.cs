﻿using DAMP.Portable.ViewModels;
using System;
using System.Linq;
using Xamarin.Forms;

namespace DAMP.Portable.Views
{
    public partial class PodcastPage : ContentPage
    {
        private Boolean sort_Desc = true;
        private string search_string = "";

        public PodcastPage()
        {
            InitializeComponent();
        }

        private PodcastViewModel ViewModel
        {
            get { return BindingContext as PodcastViewModel; }
        }


        public PodcastPage(MenuType item, RootPage root)
        {
            InitializeComponent();
            BindingContext = new PodcastViewModel(item);

            listView.ItemTapped += (sender, args) =>
            {
                if (listView.SelectedItem == null)
                    return;

                Navigation.PushAsync(new PodcastPlaybackPage(listView.SelectedItem as FeedItem, root));
                listView.SelectedItem = null;
            };

            ToolbarItems.Add(new ToolbarItem("Sort", "ic_sort.png", () =>
            {
                SortList();
            }));           
        }

        private void SortList()
        {
            sort_Desc = !sort_Desc;

            if (string.IsNullOrEmpty(search_string))
            {
                if (sort_Desc)
                {
                    listView.ItemsSource = ViewModel.FeedItems.OrderBy(o => o.Id).ToList();
                }
                else
                {
                    listView.ItemsSource = ViewModel.FeedItems.OrderByDescending(o => o.Id).ToList();
                }
            }
            else
            {
                if (sort_Desc)
                {
                    listView.ItemsSource = ViewModel.FeedItems.Where(x => x.Caption.ToLower().Contains(search_string)).OrderBy(o => o.Id).ToList();
                }
                else
                {
                    listView.ItemsSource = ViewModel.FeedItems.Where(x => x.Caption.ToLower().Contains(search_string)).OrderByDescending(o => o.Id).ToList();
                }
            }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            if (ViewModel == null || !ViewModel.CanLoadMore || ViewModel.IsBusy || ViewModel.FeedItems.Count > 0)
                return;

            ViewModel.LoadItemsCommand.Execute(null);
        }

        private void SearchBar_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (string.IsNullOrEmpty(e.NewTextValue))
            {
                search_string = "";
                listView.ItemsSource = ViewModel.FeedItems;
            }
            else
            {
                search_string = e.NewTextValue.ToLower();
                listView.ItemsSource = ViewModel.FeedItems.Where(x => x.Caption.ToLower().Contains(search_string));
            }
        }
    }
}
