﻿using System;

namespace DAMP.Portable
{
    public enum MenuType
    {
        DrewMikePodcast,
        Twitter,
        Drops,
        Sponsors
    }
    public class HomeMenuItem : BaseModel
    {
        public HomeMenuItem()
        {
            MenuType = MenuType.DrewMikePodcast;
        }
        public string Icon { get; set; }
        public MenuType MenuType { get; set; }
    }
}

