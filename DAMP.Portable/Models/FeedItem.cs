using System;
using System.ComponentModel;
using System.Text.RegularExpressions;
using Xamarin.Forms;

namespace DAMP.Portable
{
    public class FeedItem : INotifyPropertyChanged
    {
        //public string Content { get; set; }
        public string Description { get; set; }
        public string Link { get; set; }
        private string publishDate;
        public string PublishDate
        {
            get { return publishDate; }
            set
            {
                DateTime time;
                if (DateTime.TryParse(value, out time))
                    publishDate = time.ToLocalTime().ToString("D");
                else
                    publishDate = value;
            }
        }
        public string Author { get; set; }
        public string AuthorEmail { get; set; }
        public int Id { get; set; }
        public string CommentCount { get; set; }
        public string Category { get; set; }

        public string Mp3Url { get; set; }

        private string title;
        public string Title
        {
            get
            {
                return title;
            }
            set
            {
                title = value;

            }
        }

        private bool is_downloaded;
        public bool IsDownloaded
        {
            get
            {
                if (is_downloaded == false)
                {
                    String audioPath = Mp3Url;
                    string[] words = audioPath.Split('/');
                    String fileName = words[words.Length - 1];
                    var fileService = DependencyService.Get<IFiles>();
                    if (fileService.FileExists(fileName))
                    {
                        return true;
                    }
                }
                return is_downloaded;
            }
            set
            {
                is_downloaded = value;
                OnPropertyChanged("IsDownloaded");
            }
        }

        private string caption;

        public string Caption
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(caption))
                    return caption;

                
                //get rid of HTML tags
                caption = Regex.Replace(Description, "<[^>]*>", string.Empty);

                
                //get rid of multiple blank lines
                caption = Regex.Replace(caption, @"^\s*$\n", string.Empty, RegexOptions.Multiline);

                //start DAMP
                string[] words = caption.Split('\n');
                string output = words[0] + "\n\n";
                string info = words[1];
                info = info.Replace("\t", string.Empty);
                info = info.TrimStart(' ');

                caption = output + info;
                //end DAMP

                //caption = output.Substring(0, output.Length < 200 ? output.Length : 200).Trim();
                return caption;
            }
        }

        private string content;
        public string Content
        {
            get
            {
                var output = content.Insert(0, "<font face=\"arial\">");
                content = output.Insert(output.Length, "</font>");
                return content;
            }
            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    content = value;
                }
                else
                {
                    var output = Description.Insert(0, "<p style=\"padding: 20px 20px 20px 20px\">");
                    output = output.Replace("\n", "<br><br> &#8226; ");
                    output = output.Insert(output.Length, "</p>");
                    content = output;
                }
            }
        }

        public string Length { get; set; }

        private bool showImage = true;

        public bool ShowImage
        {
            get { return showImage; }
            set { showImage = value; }
        }

        private string image = @"https://secure.gravatar.com/avatar/70148d964bb389d42547834e1062c886?s=60&r=x&d=http%3a%2f%2fd1iqk4d73cu9hh.cloudfront.net%2fcomponents%2fimg%2fuser-icon.png";

        /// <summary>
        /// When we set the image, mark show image as true
        /// </summary>
        public string Image
        {
            get { return image; }
            set
            {
                image = value;
                showImage = true;
            }

        }

        private string firstImage;
        public string FirstImage
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(firstImage))
                    return firstImage;


                var regx = new Regex("http://([\\w+?\\.\\w+])+([a-zA-Z0-9\\~\\!\\@\\#\\$\\%\\^\\&amp;\\*\\(\\)_\\-\\=\\+\\\\\\/\\?\\.\\:\\;\\'\\,]*)?.(?:jpg|bmp|gif|png)", RegexOptions.IgnoreCase);
                var matches = regx.Matches(Description);

                if (matches.Count == 0)
                    firstImage = ScottHead;
                else
                    firstImage = matches[0].Value;

                return firstImage;
            }
        }

        public ImageSource FirstImageSource
        {
            get
            {
                var image = FirstImage;
                return UriImageSource.FromUri(new Uri(image));
            }
        }

        public string ScottHead { get { return "http://www.DAMP.com/images/photo-scott-tall.jpg"; } }

        private decimal progress = 0.0M;
        public decimal Progress
        {
            get { return progress; }
            set { progress = value; OnPropertyChanged("Progress"); }
        }


        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string name)
        {
            if (PropertyChanged == null)
                return;
            PropertyChanged(this, new PropertyChangedEventArgs(name));
        }
    }
}
