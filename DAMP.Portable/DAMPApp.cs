﻿using System;
using Xamarin.Forms;

namespace DAMP.Shared
{
	public static class DAMPApp
	{


		private static Page homeView;
		public static Page RootPage
		{
			get { return homeView ?? (homeView = new HomeView ()); }
		}
	}
}

