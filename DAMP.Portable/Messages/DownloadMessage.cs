﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAMP.Portable.Messages
{
    public class DownloadMessage
    {
        public string Url { get; set; }
        public string Filename { get; set; }
    }
}
