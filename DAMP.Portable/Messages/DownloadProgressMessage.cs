﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAMP.Portable.Messages
{
    public class DownloadProgressMessage
    {
        public long BytesWritten { get; set; }

        public long TotalBytesWritten { get; set; }

        public long TotalBytesExpectedToWrite { get; set; }

        public double Percentage { get; set; }
    }
}
