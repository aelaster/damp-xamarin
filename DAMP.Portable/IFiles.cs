﻿using System.Threading.Tasks;

namespace DAMP.Portable
{
    public interface IFiles
    {
        Task SaveFileAsync(string filename, byte[] data);
        Task SaveFileAsync(string filename, string text);
        Task<string> LoadFileAsync(string filename);
        bool FileExists(string filename);
        string GetDocPath(string filename);
        bool MoveFromTemp(string filename);
    }
}