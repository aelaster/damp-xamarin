﻿using System.Threading.Tasks;
using DAMP.Portable;
using Xamarin.Forms;
using System;
using System.IO;
using Foundation;
using System.Linq;
using DAMP.iOS;

[assembly: Dependency(typeof(Files_iOS))]

namespace DAMP.iOS
{
    class Files_iOS : IFiles
    {

        public static string DocumentsPath
        {
            get
            {
                var documentsDirUrl = NSFileManager.DefaultManager.GetUrls(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomain.User).Last();
                return documentsDirUrl.Path;
            }
        }

        #region IFiles implementation

        public async Task SaveFileAsync(string filename, byte[] data)
        {
            string path = CreatePathToFile(filename);
            await Task.Run(() => File.WriteAllBytes(path, data));
        }

        public async Task SaveFileAsync(string filename, string text)
        {
            string path = CreatePathToFile(filename);
            using (StreamWriter sw = File.CreateText(path))
                await sw.WriteAsync(text);
        }

        public async Task<string> LoadFileAsync(string filename)
        {
            string path = CreatePathToFile(filename);
            using (StreamReader sr = File.OpenText(path))
                return await sr.ReadToEndAsync();
        }

        public bool FileExists(string filename)
        {
            return File.Exists(CreatePathToFile(filename));
        }

        public string GetDocPath(string filename)
        {
            return Path.Combine(DocumentsPath, filename);
        }

        public bool MoveFromTemp(string filename)
        {
            return true;
        }

        #endregion

        static string CreatePathToFile(string fileName)
        {
            return Path.Combine(DocumentsPath, fileName);
        }
    }
}