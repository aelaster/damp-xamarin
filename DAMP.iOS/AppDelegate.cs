using System;
using System.Collections.Generic;
using System.Linq;
using Foundation;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using DAMP.Portable;
using ImageCircle.Forms.Plugin.iOS;
using Plugin.MediaManager.Forms.iOS;
using DAMP.Portable.Messages;
using DAMP.iOS.Services;
using CarouselView.FormsPlugin.iOS;

namespace DAMP.iOS
{
    // The UIApplicationDelegate for the application. This class is responsible for launching the
    // User Interface of the application, as well as listening (and optionally responding) to
    // application events from iOS.
    [Foundation.Register("AppDelegate")]
    public partial class AppDelegate : FormsApplicationDelegate
    {
        public static Action BackgroundSessionCompletionHandler;

        public override void HandleEventsForBackgroundUrl(UIApplication application, string sessionIdentifier, Action completionHandler)
        {
            Console.WriteLine("HandleEventsForBackgroundUrl(): " + sessionIdentifier);
            // We get a completion handler which we are supposed to call if our transfer is done.
            BackgroundSessionCompletionHandler = completionHandler;
        }
        //
        // This method is invoked when the application has loaded and is ready to run. In this
        // method you should instantiate the window, load the UI into it and then make the window
        // visible.
        //
        // You have 17 seconds to return from this method, or iOS will terminate your application.
        //
        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            UINavigationBar.Appearance.BarTintColor = UIColor.FromRGB(43, 132, 211); //bar background
            UINavigationBar.Appearance.TintColor = UIColor.White; //Tint color of button items
            UINavigationBar.Appearance.SetTitleTextAttributes(new UITextAttributes()
                {
                    Font = UIFont.FromName("HelveticaNeue-Light", (nfloat)20f),
                    TextColor = UIColor.White
                });
            Forms.Init();
            VideoViewRenderer.Init();
            ImageCircleRenderer.Init();
            CarouselViewRenderer.Init();

            App.CreateAudioPlayer = () => new SimpleAudioPlayer();

            MessagingCenter.Subscribe<DownloadMessage>(this, "Download", async message => {
                var downloader = new Downloader(message.Url, message.Filename);
                await downloader.DownloadFile();
            });

            LoadApplication(new App());
            return base.FinishedLaunching(app, options);
        }


    }
}

