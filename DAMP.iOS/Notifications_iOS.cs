﻿using System.Threading.Tasks;
using DAMP.Portable;
using Xamarin.Forms;
using System;
using System.IO;
using Foundation;
using System.Linq;
using DAMP.iOS;

[assembly: Dependency(typeof(Notifications_iOS))]

namespace DAMP.iOS
{
    class Notifications_iOS : INotifications
    {
        #region INotifications implementation

        public void StartNotification(string title, string text, int type)
        {
            throw new NotImplementedException();
        }

        public void StopNotification(int type)
        {
            throw new NotImplementedException();
        }

        public void UpdateNotification(double progress, int type)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}